package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "John", 10);
    Customer customer2 = new Customer(2, "Mary", 20);
    Customer customer3 = new Customer(3, "Peter", 5);

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> allCustomer = new ArrayList<>();

        allCustomer.add(customer1);
        allCustomer.add(customer2);
        allCustomer.add(customer3);

        return allCustomer;
    }
}
