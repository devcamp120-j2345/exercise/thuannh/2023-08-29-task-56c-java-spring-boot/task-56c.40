package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;

    Invoice Invoice1 = new Invoice(1, 100.0);
    Invoice Invoice2 = new Invoice(2, 200.0);
    Invoice Invoice3 = new Invoice(3, 300.0);

    public ArrayList<Invoice> getAllInvoices() {
        ArrayList<Invoice> allInvoice = new ArrayList<>();

        Invoice1.setCustomer(customerService.getAllCustomers().get(0));
        Invoice2.setCustomer(customerService.getAllCustomers().get(1));
        Invoice3.setCustomer(customerService.getAllCustomers().get(2));

        allInvoice.add(Invoice1);
        allInvoice.add(Invoice2);
        allInvoice.add(Invoice3);

        return allInvoice;
    }
}
