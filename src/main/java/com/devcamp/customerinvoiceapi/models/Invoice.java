package com.devcamp.customerinvoiceapi.models;

public class Invoice {
    private int id;
    private double amount;
    private Customer customer;

    public Invoice() {
    }

    public Invoice(int id, double amount) {
        this.id = id;
        this.amount = amount;
    }

    public Invoice(int id, double amount, Customer customer) {
        this.id = id;
        this.amount = amount;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getCustomerId() {
        return customer.getId();
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public int getCustomerDiscount() {
        return customer.getDiscount();
    }

    public double getAmountAfterDiscount() {
        return amount * (1 - (double) customer.getDiscount() / 100);
    }
}
